use std::process::Command;

use config::Config;
use serenity::async_trait;
use serenity::model::channel::Message;
use serenity::prelude::*;

struct Handler {
    screen_name : String,
    start_script : String,
    stop_script : String,
    backup_script : String,
}

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content == "!start" {
            if let Err(why) = msg.channel_id.say(&ctx.http, "Starting server...").await {
                println!("Error sending message: {why:?}");
            }
            let command_result = Command::new(self.start_script.to_owned())
                .status();
            if !command_result.is_ok() {
                if let Err(why) = msg.channel_id.say(&ctx.http, "Something went wrong.").await {
                    println!("Error sending message: {why:?}");
                }
            } else {
                let exit_code = command_result.unwrap();
                if exit_code.success() {
                    if let Err(why) = msg.channel_id.say(&ctx.http, "Started.").await {
                        println!("Error sending message: {why:?}");
                    }
                } else {
                    if let Err(why) = msg.channel_id.say(&ctx.http, "Failed to start. (might be running already).").await {
                        println!("Error sending message: {why:?}");
                    }
                }
            }
        }
        if msg.content == "!stop" {
            if let Err(why) = msg.channel_id.say(&ctx.http, "Stopping server...").await {
                println!("Error sending message: {why:?}");
            }
            let command_result = Command::new(self.stop_script.to_owned())
                .status();
            if !command_result.is_ok() {
                if let Err(why) = msg.channel_id.say(&ctx.http, "Something went wrong.").await {
                    println!("Error sending message: {why:?}");
                }
            } else {
                let exit_code = command_result.unwrap();
                if exit_code.success() {
                    if let Err(why) = msg.channel_id.say(&ctx.http, "Stopped.").await {
                        println!("Error sending message: {why:?}");
                    }
                } else {
                    if let Err(why) = msg.channel_id.say(&ctx.http, "Failed to stop (might not be running).").await {
                        println!("Error sending message: {why:?}");
                    }
                }
            }
        }
        if msg.content == "!running" {
            if let Err(why) = msg.channel_id.say(&ctx.http, "Checking...").await {
                println!("Error sending message: {why:?}");
            }
            let command_result = Command::new("screenfree")
                .arg(self.screen_name.to_owned())
                .status();
            if !command_result.is_ok() {
                if let Err(why) = msg.channel_id.say(&ctx.http, "Something went wrong.").await {
                    println!("Error sending message: {why:?}");
                }
            } else {
                let exit_code = command_result.unwrap();
                if exit_code.success() {
                    if let Err(why) = msg.channel_id.say(&ctx.http, "The server is not running.").await {
                        println!("Error sending message: {why:?}");
                    }
                } else {
                    if let Err(why) = msg.channel_id.say(&ctx.http, "The server is running.").await {
                        println!("Error sending message: {why:?}");
                    }
                }
            }
        }
        if msg.content == "!backup" {
            if let Err(why) = msg.channel_id.say(&ctx.http, "Starting backup. Bot will be unresponsive.").await {
                println!("Error sending message: {why:?}");
            }
            let command_result = Command::new(self.backup_script.to_owned())
                .status();
            if !command_result.is_ok() {
                if let Err(why) = msg.channel_id.say(&ctx.http, "Something went wrong.").await {
                    println!("Error sending message: {why:?}");
                }
            } else {
                let exit_code = command_result.unwrap();
                if exit_code.success() {
                    if let Err(why) = msg.channel_id.say(&ctx.http, "Backup finished.").await {
                        println!("Error sending message: {why:?}");
                    }
                } else {
                    if let Err(why) = msg.channel_id.say(&ctx.http, "Failed to start backup. (The server might be running).").await {
                        println!("Error sending message: {why:?}");
                    }
                }
            }
        }
    }
}

#[tokio::main]
async fn main() {
    let token : String;
    let screen_name : String;
    let start_script : String;
    let stop_script : String;
    let backup_script : String;

    println!("Reading config...");

    let config_result = Config::builder()
        .add_source(config::File::with_name("./bot.toml"))
        .build();

    if !config_result.is_ok() {
        println!("Could not open config file \"bot.toml\"");
        return;
    }

    let config = config_result.unwrap();
    token = config.get("token").unwrap();
    screen_name = config.get("screen_name").unwrap();
    start_script = config.get("start_script").unwrap();
    stop_script = config.get("stop_script").unwrap();
    backup_script = config.get("backup_script").unwrap();
    
    let intents = GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::DIRECT_MESSAGES
        | GatewayIntents::MESSAGE_CONTENT;

    println!("Creating bot...");
    
    let mut client =
        Client::builder(&token, intents).event_handler(Handler {screen_name, start_script, stop_script, backup_script}).await.expect("Err creating client");
    
    if let Err(why) = client.start().await {
        println!("Client error: {why:?}");
    }

    println!("Exiting main thread...");
}
